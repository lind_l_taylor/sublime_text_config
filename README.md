# README #

My ST3 config

---

### How do I get set up? ###

1. install [Sublime Text](https://www.sublimetext.com/)
2. install necessary dependencies 
3. clone this repo into **~/Library/Application Support/Sublime Text 3/** directory , owerwriting its content

---


### Necessary Dependencies ###

1. Fira Code Fonts:

	* ** brew tap caskroom/fonts ** 
	* ** brew cask install font-fira-code **
	
2. ImageMagick:

	** brew install ImageMagick **